# -*- coding: utf-8 -*-
"""
Created on Fri May  1 11:22:13 2020

@author: Casey
"""

import streamload
import nose


class TestStreamLoad(object):
    
    @classmethod
    def setup_class(cls):
        """this method is run once for each class before any tests are run"""
        cls.Q = 10.73 #m^3/s
        cls.C = 0.0586 #mg/l
     
        
    @classmethod
    def teardown_class(cls):
        """this method is run once for each class __after__ all tests are run"""
        pass
    
    def setUp(self):
        """this method is run once before __each__ tests is run"""
        pass
    
    def teardown(self):
        """this method is run once after __each__ tests is run"""
        pass
    
    def test__init(self):
        stream_load = streamload.StreamLoad(self.Q, self.C)
        
        nose.tools.assert_equal(stream_load.Q, self.Q)
        nose.tools.assert_equal(stream_load.C, self.C )

    def test_load_(self):
        load = streamload.StreamLoad(self.Q, self.C)
        
        
        stream_load = load.load_()
        solution = 0.0543314
        nose.tools.assert_almost_equal(solution, stream_load, places = 4)