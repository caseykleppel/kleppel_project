# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 10:10:50 2020

@author: Marco.Maneta
"""


from scipy.integrate import solve_ivp

class WatershedConceptual(object):
    """Implementation of mass balance based conceptual rainfall-runoff model"""
    
    def __init__(self, area, alpha, beta, dt, SSo, C):
        """
        :param area: area of the basin (sq km)
        :param alpha: basin conductance coefficient (1/s)
        :param beta: basin response paramter (dimensionless)
        :param dt: integration time step (s)
        :param SSo: initial specific basin storage (m)
        :param C: contmainant concentration (mg/L)
        :return: None
        """
        # Parameters
        self.A = area * 1e6  # Conversion from sq km to sq m
        self.alpha = alpha
        self.beta = beta
        self.C = C #mg/L
                
        # Initial condition
        self.So = SSo * self.A # Basin storage in cubic meters
        
        #  Time variables
        self.dt = dt
        self.t = 0
        
        # State variables
        self.S = [self.So]
        
        # Output variable
        self.Q = [self._calculate_discharge(self.So)]
        self.load = [self._calculate_load(self.So)]

        # Mass Balance Control
        self._acc_precip = 0
        self._acc_discharge = 0
        self._init_storage = self.So
        self._percent_mb_err = 0

        
    
    def _calculate_discharge(self, S):
        """returns discharge associated with given storage S
        
        :param S: current basin storage (cubic meters)
        :returns: discharge (cumecs)
        """
        
        return self.alpha * S ** self.beta
    
    def _calculate_load(self, S):
        """returns Load associated with give contaminant concentration C
        
        :param S: current basin storage (cubic meters)
        :param C: contaminant concentration (mg/L)
        :conversion factor: 35.31467 - converts m^3/s to ft^3/s
        :conversion factor: 5.39 converts load to lbs/day
        :returns: Load (lbs/day)
        """
        
        return   35.31467 * self.C * 5.39 * self._calculate_discharge(S) 
        
    def _change_storage(self, t, S, precip):
        """basin dS/dt function
        
        :param t: current time step (integer)
        :param S: current storage at time t (cubic meters)
        :param precip: current precipitation at time t (mm/s)
        
        :returns: dS/dt (cubmic meters per second)
        """
        
        precip_m = precip / 1000
        dSdt = precip_m * self.A - self._calculate_discharge(S)
        return dSdt
    
    def run_timestep(self, precip, solution_method=1):
        """calculate watershed discharge associated with precip
        
        :param precip: current precipitation intensity (mm/s)
        :param solution_method: solve between two solvers:
            1 Euler integrator (default)
            2 Runge-Kutta of order 4
        :returns: None
        """

        if solution_method == 1:
            initial_storage = self.S[-1]
            S = initial_storage + self._change_storage(1, initial_storage, precip) * self.dt

        elif solution_method == 2:
            initial_storage = [self.S[-1]]

            ret = solve_ivp(lambda t, S: self._change_storage(1, S, precip),
                            [0, self.dt], initial_storage, t_eval=[self.dt])
            S = ret.y[0][-1]
        else:
            print("Solution method number %i not identified" % solution_method)

        self.S.append(S)
        self.Q.append(self._calculate_discharge(S))
        self.load.append(self._calculate_load(S))
        self.t += 1










