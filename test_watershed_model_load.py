# -*- coding: utf-8 -*-
"""
Created on Fri May  1 15:46:00 2020

@author: Casey
"""


import watershed_model_load
import nose


class TestWatershedConceptual(object):
    
    @classmethod
    def setup_class(cls):
        """this method is run once for each class before any tests are run"""
        cls.area = 150  # sq kilometers
        cls.alpha = 0.00001 # 1/s
        cls.beta = 1.1 #
        cls.dt = 1800 # intgration time step in seconds (30 minutes)
        cls.SSo = 0. # one meter of initial storage
        cls.C = 0.0586 #mg/L
        
    @classmethod
    def teardown_class(cls):
        """this method is run once for each class __after__ all tests are run"""
        pass
    
    def setUp(self):
        """this method is run once before __each__ tests is run"""
        pass
    
    def teardown(self):
        """this method is run once after __each__ tests is run"""
        pass
    
    def test__init(self):
        wtshd = watershed_model_load.WatershedConceptual(self.area, 
                                                    self.alpha,
                                                    self.beta,
                                                    self.dt,
                                                    self.SSo,
                                                    self.C)
        nose.tools.assert_equal(wtshd.A, self.area * 1e6)
        nose.tools.assert_equal(wtshd.alpha, self.alpha)
        nose.tools.assert_equal(wtshd.beta, self.beta)
        nose.tools.assert_equal(wtshd.dt, self.dt)
        nose.tools.assert_equal(wtshd.S[0], self.SSo * self.area * 1e6)
        nose.tools.assert_equal(wtshd.C, self.C)
        
        
    def test_run_timestep(self):
        wtshd = watershed_model_load.WatershedConceptual(self.area,
                                                    self.alpha,
                                                    self.beta,
                                                    self.dt,
                                                    self.SSo, self.C)
        precip = 5.55556E-03
        expected = ((5.55556E-03 / 1000 * self.area * 1e6) / self.alpha) ** (1/self.beta)
        for i in range(1000):
            wtshd.run_timestep(precip)

        nose.tools.assert_almost_equal(expected, wtshd.S[-1])

    def test_run_timestep2(self):
        wtshd = watershed_model_load.WatershedConceptual(self.area,
                                                    self.alpha,
                                                    self.beta,
                                                    self.dt,
                                                    self.SSo,
                                                    self.C)
        precip = 5.55556E-03
        expected = ((5.55556E-03 / 1000 * self.area * 1e6) / self.alpha) ** (1 / self.beta)
        for i in range(1000):
            wtshd.run_timestep(precip, solution_method=2)

        nose.tools.assert_almost_equal(expected, wtshd.S[-1])

        