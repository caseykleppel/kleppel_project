# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 16:58:04 2020

@author: Casey
"""

class StreamLoad(object):
 

    def __init__(self, Q, C):
        
        """
        Parameters
        ----------
        Discharge : Stream discharge for 1 day (m^3 / s)
        
        contaminant_concentration : concentration of contaminant in stream for 1 day (mg/L)
        
              
        Returns Load of stream contaminant in metric tons
        -------
        None."""
        
        self.Q = Q
        self.C = C
        
      
        
        ### Create a list to store Load in
        self.Load = []
    

    def load_(self):
        """returns stream load lbs/day
        
        :param C: concentration of contaminant or sediment, mg/L
        :param Q: Discharge of stream, m^3/s
        :returns: Load, metric tons
        """
        
        L = self.Q * self.C  * 35.31467 * 0.0024468
        self.Load.append(L) 
        return L 